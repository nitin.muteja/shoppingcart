package tw.unitests.unit;
import org.junit.Test;
import tw.controller.BookController;
import tw.models.Book;

import static org.junit.Assert.assertEquals;


public class BookControllerTest {

    @Test
    public void should_return_book_name(){
        BookController controller=new BookController();
        Book response=controller.index();
        assertEquals("Test",response.getName());
    }

}
