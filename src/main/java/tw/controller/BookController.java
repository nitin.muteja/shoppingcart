package tw.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import tw.models.Book;

@RestController
public class BookController {
    
    @RequestMapping("/book")
    public Book index() {
        return new Book("Test");
    }
    
}
